General Procedure for MARS downloads and processing.

1. Download from MARS

The downloads from MARS can be quite slow, so be prepared.

The first step is to use the *.template files in the templates directory
to construct .json templates that can be used for the download(s). 
.json files are used as the specification to the MARS API.

Typical values that need to be updated in the template files are:

* "grid"  - Typically 0.125 or 0.4.

* "param"  - Specifies which params are being requested. It's 
inconvenient, but usually best, to use param number id's instead of 
names because sometimes MARS can't differentiate between two params 
with similar names.

* "stream" - The "oper" stream is used for 00/12z cycles for non-ensemble
data. "scda" is for the 06/18z cycles for the same non-ensemble data. The
"enfo" stream is used for ensemble data (all cycles). 

* "time" - The cycle time.

* "date" - The date, in the form YYYY-MM-DD.

* "levtype" - The level type. Surface ("sfc") or Model Level ("ml").

* "levelist" - If "levtype" is "ml" the the levels desired are listed here.

* "type" - The type of data. Forecast "fc" or analysis "an".

* "class" - Always "od".

* "step" - The leads/steps.

* "target" - The target/destination file for the ingested data. A subdirectory
can be specified if desired.

In general, params can either be a single value, or multiple values 
differentiated by "/". For example, "param": 75/76/130. A series can also
be specified by using "/" and "to" and "by". For example, to specify 3 hourly
leads from f003 to f054 (inclusive): "step", 3/to/54/by/3.

Once a satisfactory template file is constructed, make the appropriate updates
in generate-jsons.sh and run it. This will create all the needed .jsons in the
jsons subdir.

Finally, once the jsons are ready, activate the DEV-ecmwf-ingest conda environment
inside a tmux or screens session (tmux or screens is required so that the ingest
process doesn't time out).

Make any updates to hopper.sh ("keep the hopper full") and run it. Hopper will 
submit jobs to the ECMWF MARS API as long as there are still unsubmitted jsons
left. Although ECMWF allows up to 20 jobs to be queued at once, it was found that
if more than about 8 at time were queued, and the jobs were big, those jobs at the
end of the queue would time out. Currently, ECMWF only allows 2 jobs to be active
at one time. Job status can be checked at https://apps.ecmwf.int/webmars/joblist/.

When all the jsons have been submitted, and all the requests have been satisfied, the
raw MARS files will be in mars_raw (or where ever the "target" param directed they go).

2. Create the "processed" dir structure.

It's best to create a parallel processed dir structure before modifying data/processed.
Make the appropriate updates to create-date-dirs.sh and run it to create a local
processed directory structure.

The following instructions are for the most "straightline" application, i.e. one where
all data fit in one < 75GB file, all params are homogeneous GRIB types, etc.

3. Split raw MARS files into their constituent parts.

Raw MARS files contain all the GRIB data in a densely packed structure. For instance,
one raw MARS file may contain multiple cycles, multiple leads, and multiple days. In
order to process these files efficiently into the standard NOAA naming convention, these
raw files need to be "split" so that each split file contains a subset of the raw file.

This splitting is done via the eccodes grib_filter. The grib_filter executable uses a 
"filter" file as a template for how to break down a packed GRIB file. These filter files
are in the top directory here and are denoted by a .filter suffix.

Each filter file writes a new file, based on the MARS data specified within the "[]". 
For example, "[dataDate]" denotes the date for the data.

Make the appropriate changes in the bin/eccodes-filter.sh file, and execute it. After
operating on all the raw files, the split directory will contain the broken down individual
files. Note that to execute eccodes-filter, one needs to be in a conda environment that
contains grib_filter (e.g. the latest hera conda environment).

Note it is best to run the grib_filter on an interactive batch node.

3.5 Combine/cat grib messages, if required.

Sometimes, when MARS files are split in step 3, the result will be some files that span 
two leadtimes. For example, a split file might be of the name ecmwf.t0000z.pgrb.f009-10.20211120.
This indicates that params within that file are applicable to the range of the leads. But
this param needs to be in a file with all the other leads. So, using the last lead as the lead
of interest, this multi-lead file needs to be combined (the bash cat command is fine) with
the single lead file. The catleads.sh script does this.


4. Link from the split directory into processed.

After step 3 above, the split directory will contain all the GRIB files broken down into
useable smaller files. But, these individual files need to be placed into an appropriate
directory tree of the standard format (i.e. directories of the form YYYYMMDD/HH). The
bin/link-to-processed-and-rename.sh script does this. After updating the script, run it to
link all the files from split into the processed tree. 

