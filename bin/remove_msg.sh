#!/bin/sh

for file in $(find processed/202111{21..24} -name "ecmwf.*"); do
    echo "grib_copy -w \"shortName!=ssrd\" ${file} tmp"
    grib_copy -w "shortName!=ssrd" ${file} tmp
    echo "mv tmp ${file}"
    mv tmp ${file}
    echo
done
