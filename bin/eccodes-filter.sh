#!/bin/bash

FILTER_FILE="ecmwf.ml.filter"
MARS_FILES="ecmwf.*"

# eccodes-filter
#
# Use the eccodes grib_filter and a filter spec file to split
# a raw MARS file into its constituent parts.

grib_filter -v ${FILTER_FILE} mars_raw/${MARS_FILES} 
