#!/bin/sh

# for date in 2021-10-{21..31} 2021-11-{01..30}; do
# for date in 2021-10-{22..31}; do
# for date in 2021-10-20; do
# for date in 2021-10-20; do
for date in 2021-11-{07..30}; do

  echo $date

  nohyphendate=$(echo $date | tr -d '-')

  echo $nohyphendate

  echo "creating json"
  echo

  for file in $(ls ecmwf.ml.t*z.pgrb.0p125.f*.YYYYMMDD.json.template); do
    cp $file jsons/${file/YYYYMMDD/$nohyphendate}
  done

  rename .template '' jsons/ecmwf.ml.t*z.pgrb.0p125.f*.$nohyphendate.json.template

  for file in $(ls jsons/ecmwf.ml.t*z.pgrb.0p125.f*.${nohyphendate}.json); do
    sed -i -e "s/YYYY-MM-DD/$date/" $file
    sed -i -e "s/YYYYMMDD/$nohyphendate/" $file
  done

done
