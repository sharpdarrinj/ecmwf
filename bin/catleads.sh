#!/bin/bash

# Fold in/collapse a grib file with a range of leads (i.e. f000-1) into an existing
# grib file. Use the second lead time in the range as the file to fold into. For
# example, f002-3 folds into f002.

for file in $(find split -type f -name "ecmwf.*-*.20211120" -printf "%P\n"); do
  echo $file
  lastlead=$(echo $file | awk -F- '{print $2}')
  # echo $lastlead
  lastlead=${lastlead%%.2021????}
  lastlead="00${lastlead}"
  date=$(echo $file | awk -F. '{print $6}')
  # echo $lastlead
  strippedfilename=${file%%.f*}
  echo "cat split/$file split/${strippedfilename}.f${lastlead}.$date >> catted_split/${strippedfilename}.f${lastlead}.$date"
  #cat split/$file split/${strippedfilename}.f${lastlead}.$date >> catted_split/${strippedfilename}.f${lastlead}.$date
  echo
done
