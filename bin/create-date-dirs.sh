#!/bin/bash

# create-date-dirs
#
# Create a directory structure in the processed subdir.
# Adjust the dates and/or cycles as needed.

CYCLES="00 06 12 18"

for date in 202110{28..30} 202111{01..30}; do
  for cycle in ${CYCLES}; do
     echo "mkdir -p processed/${date}/${cycle}"
     mkdir -p processed/${date}/${cycle}
  done # cycle
done # date
