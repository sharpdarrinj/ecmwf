#!/bin/bash

# link-to-processed-and-rename
#
# Link files from the split directory to the processed directory,
# renaming in the process.

for file in $(ls split_pgrb2_20220125/ecmwf.t*z.pgrb2.0p125.f*.* | sort); do
  echo $file
  cycle=$(echo $file | awk -F. '{print $2}' | cut -c2-3)
  lead=$(echo $file | awk -F. '{print $5}')
  date=$(echo $file | awk -F. '{print $6}')
  dstfile="ecmwf.t${cycle}z.pgrb2.0p125.f${lead: -3}"
  echo "ln $file processed/$date/$cycle/${dstfile}"
  # ln $file processed/$date/$cycle/${dstfile}
  echo
done # file
