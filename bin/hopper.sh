#!/bin/sh

numberactive ()
{

  local numrequests=$(ps -ef | grep darrin | grep "unbuffer simple-ingest" | grep -v "grep"| wc -l)

  echo "$numrequests"

}

# main

na=0

# for file in $(ls jsons/ecmwf.ml.t??z.*.202110{22..31}.json); do
# for file in $(ls jsons/ecmwf.ml.t??z.*.20211020.json); do
# for file in $(ls jsons/ecmwf.pf.t0012z.*.202111{03..06}.json jsons/ecmwf.pf.t0618z.*.2021*.json); do
# for file in $(ls jsons/ecmwf.[tcp]*.20211020.json jsons/jsons/ecmwf.ml.t06z.pgrb.0p125.f030-054.20211106.json jsons/jsons/ecmwf.ml.t06z.pgrb.0p125.f003-027.20211105.json); do
for file in $(ls jsons/ecmwf.ml.t??z.pgrb.0p125.f*.202111{07..30}.json); do
  echo "na is $na"
  while [[ na -ge 8 ]]; do
    echo "sleeping 300 secs"
    sleep 300
    na=$(numberactive)
  done # na
  echo "getting $file"
  unbuffer simple-ingest ${file} > ${file}.out 2>&1 &
  # pause to make sure it shows up in process list
  sleep 3 
  na=$(numberactive)
done # file
