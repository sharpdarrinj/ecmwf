#!/bin/bash

# split-grib1-grib2
#
# Sometimes MARS raw data contains both GRIB1 and GRIB2 parameters.
# But, the processed files need to be exclusively one or the other. So
# split the combined 1/2 file into separate GRIB1 and GRIB2 files.

# This example splits the tprate GRIB2 parameter into a pgrb2 file. All
# other params go into pgrb files.

for file in $(ls mars_raw/ecmwf.t0618z.* | sort); do
  echo $file
  date=$(echo $file | awk -F. '{print $5}')
  echo "grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}"
  grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}
  echo "grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}"
  grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}
  echo
done
