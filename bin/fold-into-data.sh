#!/bin/bash

DATA=/fs/project/PYS0343/data/ecmwf

# fold-into-data
#
# Fold a newly created GRIB file into an existing one in the $DATA
# directory. Note the ">>" so that the file is appended and doesn't
# replace the existing file.

for file in $(find processed -type f -name "ecmwf.t06z.*" -o -name "ecmwf.t18z.*" | sort); do
  echo $file
  if [[ -f ${DATA}/${file} ]]; then
    # File already exists, so append.
    echo "cat $file >> ${DATA}/$file"
    cat $file >> ${DATA}/$file
  else
    # File does not exist, so copy/create it.
    echo "cp $file ${DATA}/$file"
    cp $file ${DATA}/$file
  fi
  echo
done
