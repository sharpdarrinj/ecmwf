#!/bin/bash

DATA=/fs/project/PYS0343/data/ecmwf
# DATA=/fs/ess/scratch/PYS0343/darrinjsharp/mld-530/data/ecmwf

create_date_dirs () {
 
  for date in 202110{22..31} 202111{01..30}; do
    for cycle in 00 06 12 18; do
       echo "mkdir -p processed/${date}/${cycle}"
       mkdir -p processed/${date}/${cycle}
    done # cycle
  done # date
}
 
link_and_rename() {
  # core forecasts
# for file in $(ls split/ecmwf.t*z.pgrb.0p125.f*.20211020 | sort); do
#   echo $file
#   cycle=$(echo $file | awk -F. '{print $2}' | cut -c2-3)
#   lead=$(echo $file | awk -F. '{print $5}')
#   date=$(echo $file | awk -F. '{print $6}')
#   dstfile="ecmwf.t${cycle}z.pgrb.0p125.f${lead: -3}"
#   echo "ln $file processed/$date/$cycle/${dstfile}"
#   ln $file processed/$date/$cycle/${dstfile}
#   echo
# done # file
#
#  # ensembles
# for file in $(ls split/ecmwf.e*.20211020 | sort); do
#   echo $file
#   ensnum=$(echo $file | awk -F. '{print $2}')
#   cycle=$(echo $file | awk -F. '{print $3}' | cut -c2-3)
#   lead=$(echo $file | awk -F. '{print $6}')
#   date=$(echo $file | awk -F. '{print $7}')
#   dstfile="ecmwf.e${ensnum: -3}.t${cycle}z.pgrb.0p4.f${lead: -3}"
#   echo "ln $file processed/$date/$cycle/${dstfile}"
#   ln $file processed/$date/$cycle/${dstfile}
#   echo
# done # file
#
# model levels
  for file in $(ls split/ecmwf.ml.*202111{01..06} | sort); do
    echo $file
    cycle=$(echo $file | awk -F. '{print $3}' | cut -c2-3)
    lead=$(echo $file | awk -F. '{print $6}')
    date=$(echo $file | awk -F. '{print $7}')
    dstfile="ecmwf.t${cycle}z.pgrb2.0p125.f${lead: -3}_ml"
    echo "ln $file processed/$date/$cycle/${dstfile}"
    ln -f $file processed/$date/$cycle/${dstfile}
    echo
  done # file
}

#link_and_rename_pgrb2() {
#
#  for file in $(ls split_pgrb2/ecmwf.*.202111?? | sort); do
#    echo $file
#    cycle=$(echo $file | awk -F. '{print $2}' | cut -c2-3)
#    lead=$(echo $file | awk -F. '{print $5}')
#    date=$(echo $file | awk -F. '{print $6}')
#    dstfile="ecmwf.t${cycle}z.pgrb2.0p125.f${lead: -3}"
#    echo "ln $file processed/$date/$cycle/${dstfile}"
#    ln $file processed/$date/$cycle/${dstfile}
#    echo
#  done # file
#}
#
#link_and_rename_pgrb() {
#
#  # for file in $(ls catted_pgrb/ecmwf.* | sort); do
#  for file in $(ls catted_pgrb/ecmwf.*.f000.* | sort); do
#    echo $file
#    cycle=$(echo $file | awk -F. '{print $2}' | cut -c2-3)
#    lead=$(echo $file | awk -F. '{print $5}')
#    date=$(echo $file | awk -F. '{print $6}')
#    dstfile="ecmwf.t${cycle}z.pgrb.0p125.f${lead: -3}"
#    echo "ln $file processed/$date/$cycle/${dstfile}"
#    ln $file processed/$date/$cycle/${dstfile}
#    echo
#  done # file
#}

#fold_in_pgrb2() {
#
#  for file in $(find processed -name "*pgrb2*" | sort); do
#    echo $file
#    if [[ -f ${DATA}/${file} ]]; then
#      echo "cat $file >> ${DATA}/$file"
#      cat $file >> ${DATA}/$file
#    else
#      echo "cp $file ${DATA}/$file"
#      cp $file ${DATA}/$file
#    fi
#  done
#}
 
fold_into_data() {

  for file in $(find processed -type f -name "ecmwf.t06z.*" -o -name "ecmwf.t18z.*" | sort); do
    #echo $file
    if [[ -f ${DATA}/${file} ]]; then
      echo "cat $file >> ${DATA}/$file"
      cat $file >> ${DATA}/$file
    else
      echo "cp $file ${DATA}/$file"
      cp $file ${DATA}/$file
    fi
    echo
  done
}
 

split_grib1_grib2() {
  for file in $(ls mars_raw/ecmwf.t0618z.* | sort); do
    echo $file
    date=$(echo $file | awk -F. '{print $5}')
    echo "grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}"
    grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}
    echo "grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}"
    grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}
    echo
  done
}

cat_leads() {
  for date in 202111{01..06}; do
    for cycle in 00 06 12 18; do
      # for file in $(ls mars_raw/ecmwf.ml.t${cycle}z.*.${date}); do
        echo "cat mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.f003-027.${date} mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.f030-054.${date} > mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.${date}" 
        cat mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.f003-027.${date} mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.f030-054.${date} > mars_raw/ecmwf.ml.t${cycle}z.pgrb.0p125.${date}
        # date=$(echo $file | awk -F. '{print $5}')
        # echo "grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}"
        # grib_copy -w shortName=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb2.0p125.${date}
        # echo "grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}"
        # grib_copy -w shortName!=tprate $file mars_grib1-2/ecmwf.t0618z.pgrb.0p125.${date}
        # echo
      # done # file
    done # cycle
  done # date
}


eccodes_filter() {

  echo "grib_filter -v ecmwf.pgrb.filter mars_grib1-2/ecmwf.*.pgrb.*" 
  echo "grib_filter -v ecmwf.pgrb2.filter mars_grib1-2/ecmwf.*.pgrb2.*" 

}

#
# MAIN starts here
#

#
# cat_leads
# split_grib1_grib2
# eccodes_filter
# create_date_dirs
# link_and_rename_pgrb2
# link_and_rename_pgrb
link_and_rename
# fold_in_pgrb2
# fold_in_pgrb
# fold_into_data
